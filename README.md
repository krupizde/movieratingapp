# MovieRatingApp

Repository pro semestrální práci z předmětu TJV. Jedná se o aplikaci pro hodnocení filmů (či jiných videí nahraných uživateli) 
s REST back-endem v Javě napsaným pomocí Spring frameworku a hibernate, komunikující s Oracle databází a front-endem napsaným 
v javasript frameworku React.