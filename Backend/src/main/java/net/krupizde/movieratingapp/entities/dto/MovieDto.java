package net.krupizde.movieratingapp.entities.dto;


public class MovieDto {
	
	private long idStudio;
	private long id;
	private String name;
	private int lengthMinutes;
	public long getIdStudio() {
		return idStudio;
	}
	public void setIdStudio(long idStudio) {
		this.idStudio = idStudio;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLengthMinutes() {
		return lengthMinutes;
	}
	public void setLengthMinutes(int lengthMinutes) {
		this.lengthMinutes = lengthMinutes;
	}
	public MovieDto(long idStudio, long id, String name, int lengthMinutes) {
		super();
		this.idStudio = idStudio;
		this.id = id;
		this.name = name;
		this.lengthMinutes = lengthMinutes;
	}
	public MovieDto() {
		super();
	}
	
}
