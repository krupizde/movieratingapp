package net.krupizde.movieratingapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import net.krupizde.movieratingapp.entities.dto.UserRatingDto;
import net.krupizde.movieratingapp.entities.ids.UserRatingsId;

@Entity
@Table(name = "tjv_user_ratings")
public class UserRating implements Serializable {

	private static final long serialVersionUID = 2L;

	@EmbeddedId
	private UserRatingsId id;

	@ManyToOne
	@MapsId("idUser")
	@JoinColumn(name = "id_user")
	private User user;

	@ManyToOne()
	@MapsId("idMovie")
	@JoinColumn(name = "id_movie")
	private Movie movie;

	@Column(name = "favorite", nullable = true)
	private Boolean favorite = false;

	@Column(name = "percentage")
	private Integer percentage;

	public UserRating() {
	}
	
	

	public UserRating(UserRatingsId id, User user, Movie movie, Boolean favorite, Integer percentage) {
		super();
		this.id = id;
		this.user = user;
		this.movie = movie;
		this.favorite = favorite;
		this.percentage = percentage;
	}



	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Boolean getFavorite() {
		return favorite;
	}

	public void setFavorite(Boolean favorite) {
		this.favorite = favorite;
	}

	public UserRatingsId getId() {
		return id;
	}

	public void setId(UserRatingsId id) {
		this.id = id;
	}

	public boolean isFavorite() {
		return favorite;
	}

	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		UserRating that = (UserRating) o;

		if (id != null ? !id.equals(that.id) : that.id != null)
			return false;

		return true;
	}

	public int hashCode() {
		return (id != null ? id.hashCode() : 0);
	}
	
	public UserRatingDto reduce() {
		return new UserRatingDto(id.getIdUser(), id.getIdMovie(), percentage, favorite);
	}
}
