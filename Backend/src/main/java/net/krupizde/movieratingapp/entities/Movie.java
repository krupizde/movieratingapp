/**
 * 
 */
package net.krupizde.movieratingapp.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import net.krupizde.movieratingapp.entities.dto.MovieDto;

/**
 * @author Zdeněk Krupička (radamanak@gmail.com)
 *
 */
@Entity
@Table(name = "tjv_movies")
public class Movie implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne()
	@JoinColumn(name = "id_studio")
	private Studio studio;

	@OneToMany(mappedBy = "movie")
	private Collection<Image> images = new ArrayList<>();

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "tjv_movie_has_tag", joinColumns = { @JoinColumn(name = "id_movie") }, inverseJoinColumns = {
			@JoinColumn(name = "id_tag") })
	private Set<Tag> tags = new HashSet<>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_movie", updatable = false, nullable = false)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "length_minutes", nullable = true)
	private Integer lengthMinutes;

	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
	@JsonIgnoreProperties(value = { "user", "movie" })
	private Set<UserRating> userRatings = new HashSet<UserRating>(0);

	@OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
	@JsonIgnoreProperties(value = { "user", "movie" })
	private Set<Comment> comments = new HashSet<>(0);

	public Movie() {

	}

	public Movie(Studio studio, Collection<Image> images, Set<Tag> tags, Long id, String name, Integer lengthMinutes,
			Set<UserRating> userRatings, Set<Comment> comments) {
		super();
		this.studio = studio;
		this.images = images;
		this.tags = tags;
		this.id = id;
		this.name = name;
		this.lengthMinutes = lengthMinutes;
		this.userRatings = userRatings;
		this.comments = comments;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<UserRating> getUserRatings() {
		return userRatings;
	}

	public void setUserRatings(Set<UserRating> userRatings) {
		this.userRatings = userRatings;
	}

	public Collection<Image> getImages() {
		return images;
	}

	public void setImages(Collection<Image> images) {
		this.images = images;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public Studio getStudio() {
		return studio;
	}

	public void setStudio(Studio studio) {
		this.studio = studio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLengthMinutes() {
		return lengthMinutes;
	}

	public void setLengthMinutes(Integer lengthMinutes) {
		this.lengthMinutes = lengthMinutes;
	}

	public MovieDto reduce() {
		return new MovieDto(studio.getId(), id, name, lengthMinutes);
	}

}
