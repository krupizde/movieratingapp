package net.krupizde.movieratingapp.entities.dto;

import java.util.Date;


public class CommentDto {
	
	private long id;
	private long idUser;
	private long idMovie;
	private String content;
	private Date timeAdded;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	public long getIdMovie() {
		return idMovie;
	}
	public void setIdMovie(long idMovie) {
		this.idMovie = idMovie;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getTimeAdded() {
		return timeAdded;
	}
	public void setTimeAdded(Date timeAdded) {
		this.timeAdded = timeAdded;
	}
	public CommentDto(long id, long idUser, long idMovie, String content, Date timeAdded) {
		super();
		this.id = id;
		this.idUser = idUser;
		this.idMovie = idMovie;
		this.content = content;
		this.timeAdded = timeAdded;
	}
	public CommentDto() {
		super();
	}
	
	
}
