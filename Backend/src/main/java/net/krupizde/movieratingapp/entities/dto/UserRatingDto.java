package net.krupizde.movieratingapp.entities.dto;

public class UserRatingDto {

	private long movieId;
	private int rating;
	private boolean favorite;
	private long userId;
	
	public UserRatingDto(long userId, long movieId, int rating, boolean favorite) {
		super();
		this.userId = userId;
		this.movieId = movieId;
		this.rating = rating;
		this.favorite = favorite;
	}
	public long getMovieId() {
		return movieId;
	}
	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public boolean isFavorite() {
		return favorite;
	}
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}
	
	
}
