package net.krupizde.movieratingapp.entities.dto;

public class ImageDto {

	private long id;
	private long idMovie;
	private String path;
	private boolean thumbnail;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdMovie() {
		return idMovie;
	}
	public void setIdMovie(long idMovie) {
		this.idMovie = idMovie;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public boolean isThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(boolean thumbnail) {
		this.thumbnail = thumbnail;
	}
	public ImageDto(long id, long idMovie, String path, boolean thumbnail) {
		super();
		this.id = id;
		this.idMovie = idMovie;
		this.path = path;
		this.thumbnail = thumbnail;
	}
	public ImageDto() {
		super();
	}
	
}
