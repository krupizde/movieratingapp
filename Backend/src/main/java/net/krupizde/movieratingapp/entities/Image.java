/**
 * 
 */
package net.krupizde.movieratingapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.krupizde.movieratingapp.entities.dto.ImageDto;

/**
 * @author Zdeněk Krupička (radamanak@gmail.com)
 *
 */
@Entity
@Table(name = "tjv_image")
public class Image implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_image", updatable = false, nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_movie", nullable = false)
	private Movie movie;

	@Column(name = "path_to_file")
	private String path;

	@Column(name = "is_thumbnail")
	private boolean isThumbnail;

	public Image() {

	}

	public Image(Long id, Movie movie, String path, boolean isThumbnail) {
		super();
		this.id = id;
		this.movie = movie;
		this.path = path;
		this.isThumbnail = isThumbnail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isThumbnail() {
		return isThumbnail;
	}

	public void setThumbnail(boolean isThumbnail) {
		this.isThumbnail = isThumbnail;
	}

	public ImageDto reduce() {
		return new ImageDto(id, movie.getId(), path, isThumbnail);
	}
}
