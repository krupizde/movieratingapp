package net.krupizde.movieratingapp.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import net.krupizde.movieratingapp.entities.dto.TagDto;

@Entity
@Table(name = "tjv_tags")
public class Tag implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tag")
	private Long id;

	@ManyToMany(mappedBy = "tags")
	private Set<Movie> movies = new HashSet<>();

	@Column(name = "name")
	private String name;

	public Tag() {

	}

	public Tag(Long id, Set<Movie> movies, String name) {
		super();
		this.id = id;
		this.movies = movies;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idTag) {
		this.id = idTag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TagDto reduce() {
		return new TagDto(id, name);
	}

}
