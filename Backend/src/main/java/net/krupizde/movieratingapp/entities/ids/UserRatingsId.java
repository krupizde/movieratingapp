/**
 * 
 */
package net.krupizde.movieratingapp.entities.ids;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import net.krupizde.movieratingapp.entities.Movie;
import net.krupizde.movieratingapp.entities.User;

/**
 * @author Zdeněk Krupička (radamanak@gmail.com)
 *
 */
@Embeddable
public class UserRatingsId implements Serializable {

	private static final long serialVersionUID = 3L;

	@Column(name = "id_user")
	private Long idUser;

	@Column(name = "id_movie")
	private Long idMovie;

	public UserRatingsId() {
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public Long getIdMovie() {
		return idMovie;
	}

	public void setIdMovie(Long idMovie) {
		this.idMovie = idMovie;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		UserRatingsId that = (UserRatingsId) o;
		return Objects.equals(idUser, that.idUser) && Objects.equals(idMovie, that.idMovie);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idMovie, idUser);
	}

}
