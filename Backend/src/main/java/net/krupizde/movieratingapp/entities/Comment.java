/**
 * 
 */
package net.krupizde.movieratingapp.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.convert.JodaTimeConverters.DateTimeToDateConverter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import net.krupizde.movieratingapp.entities.dto.CommentDto;
import net.krupizde.movieratingapp.jackson.serializers.CommentSerializer;

/**
 * @author Zdeněk Krupička (radamanak@gmail.com)
 *
 */
@Entity
@Table(name = "tjv_comments")
@JsonSerialize(using = CommentSerializer.class)
public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_comment", updatable = false, nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_user", nullable = false)
	private User user;

	@ManyToOne
	@JoinColumn(name = "id_movie", nullable = false)
	private Movie movie;

	@Column(name = "content")
	private String content;

	@Column(name = "time_added")
	private Date timeAdded;

	public Comment() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getTimeAdded() {
		return timeAdded;
	}

	public void setTimeAdded(Date timeAdded) {
		this.timeAdded = timeAdded;
	}

	public Comment(Long id, User user, Movie movie, String content, Date timeAdded) {
		super();
		this.id = id;
		this.user = user;
		this.movie = movie;
		this.content = content;
		this.timeAdded = timeAdded;
	}

	public CommentDto reduce() {
		return new CommentDto(id, user.getId(), movie.getId(), content, timeAdded);
	}

}
