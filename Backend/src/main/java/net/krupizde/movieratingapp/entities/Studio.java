/**
 * 
 */
package net.krupizde.movieratingapp.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import net.krupizde.movieratingapp.entities.dto.StudioDto;

/**
 * @author Zdeněk Krupička (radamanak@gmail.com)
 *
 */
@Entity
@Table(name = "tjv_studios")
public class Studio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_studio", updatable = false, nullable = false)
	private Long id;

	@Column(name = "name", updatable = true, unique = true, nullable = false)
	private String name;

	public Studio() {

	}

	public Studio(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long idStudio) {
		this.id = idStudio;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StudioDto reduce() {
		return new StudioDto(id, name);
	}

}
