package net.krupizde.movieratingapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.krupizde.movieratingapp.entities.Image;


public interface ImageRepository extends JpaRepository<Image, Long>{

}
