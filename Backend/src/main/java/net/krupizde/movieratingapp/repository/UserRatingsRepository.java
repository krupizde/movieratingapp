package net.krupizde.movieratingapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.krupizde.movieratingapp.entities.UserRating;
import net.krupizde.movieratingapp.entities.ids.UserRatingsId;

public interface UserRatingsRepository extends JpaRepository<UserRating, UserRatingsId>{

}
