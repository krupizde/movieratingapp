/**
 * 
 */
package net.krupizde.movieratingapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.krupizde.movieratingapp.entities.User;

/**
 * @author Zdenek Krupicka (MrScruf) radamanak@gmail.com
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	public List<User> findByUsername(String username);

}
