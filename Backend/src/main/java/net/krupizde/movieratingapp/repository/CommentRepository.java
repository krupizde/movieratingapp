package net.krupizde.movieratingapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import net.krupizde.movieratingapp.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long>{

	public List<Comment> findByMovieIdOrderByTimeAdded(long id);
}
