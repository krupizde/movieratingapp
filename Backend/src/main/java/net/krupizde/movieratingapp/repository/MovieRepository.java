package net.krupizde.movieratingapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.krupizde.movieratingapp.entities.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>{

	List<Movie> findByUserRatingsUserId(long id);
	
	List<Movie> findByName(String name);
}
