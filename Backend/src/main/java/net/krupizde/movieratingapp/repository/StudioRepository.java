package net.krupizde.movieratingapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import net.krupizde.movieratingapp.entities.Studio;

public interface StudioRepository extends JpaRepository<Studio, Long>{

	public List<Studio> findByName(String name);
}
