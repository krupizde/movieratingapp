package net.krupizde.movieratingapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.krupizde.movieratingapp.entities.Tag;

public interface TagRepository extends JpaRepository<Tag, Long>{

}
