package net.krupizde.movieratingapp.rest.controllers;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.krupizde.movieratingapp.entities.Studio;
import net.krupizde.movieratingapp.entities.dto.StudioDto;
import net.krupizde.movieratingapp.service.StudioService;
import net.krupizde.movieratingapp.service.ValidationService;

@RestController
@RequestMapping(path = "/studios")
public class StudioController {

	@Autowired
	StudioService studioService;
	@Autowired
	ValidationService validation;

	@RequestMapping(method = RequestMethod.POST)
	public Studio add(@RequestBody StudioDto studio, HttpServletResponse response, HttpSession session)
			throws IllegalAccessException {
		validation.validateAccess(session, response);
		try {
			studioService.add(new Studio(studio.getId(), studio.getName()));
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Message", e.getMessage());
			return null;
		}
		response.setStatus(HttpServletResponse.SC_OK);
		return studioService.findByName(studio.getName());
	}
}
