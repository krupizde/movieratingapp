package net.krupizde.movieratingapp.rest.controllers;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.krupizde.movieratingapp.entities.User;
import net.krupizde.movieratingapp.entities.dto.UserDto;
import net.krupizde.movieratingapp.entities.dto.UserRatingDto;
import net.krupizde.movieratingapp.service.PasswordService;
import net.krupizde.movieratingapp.service.UserRatingsService;
import net.krupizde.movieratingapp.service.UserService;
import net.krupizde.movieratingapp.service.ValidationService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	PasswordService passwordService;

	@Autowired
	UserRatingsService userRatingsService;

	@Autowired
	ValidationService validation;

	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public void login(@RequestBody UserDto userDto, HttpSession session, HttpServletResponse response)
			throws NoSuchAlgorithmException {
		String passwordHash = passwordService.hashPassword(userDto.getPassword());
		User user = userService.findByUsername(userDto.getUsername());
		boolean exist = passwordHash.equals(user.getPasswordHash());
		if (exist) {
			session.setAttribute("user", user);
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Message", "Wrong username or password");
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	public void register(@RequestBody UserDto user, HttpServletResponse response) {
		if (user.getPassword().isEmpty() || user.getUsername().isEmpty()) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Message", "Password or username must not be empty");
			return;
		}
		String passwordHash = passwordService.hashPassword(user.getPassword());
		try {
			userService.register(new User(new Long(0), user.getUsername(), passwordHash, null, null));
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Message", e.getMessage());
			return;
		}
		response.setStatus(HttpServletResponse.SC_CREATED);
	}

	@RequestMapping(path = "/rating", method = RequestMethod.POST)
	public void rate(@RequestBody UserRatingDto mRate, HttpSession session, HttpServletResponse response)
			throws IllegalAccessException {
		validation.validateAccess(session, response);
		try {
			userRatingsService.addRating(mRate, (User) session.getAttribute("user"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.setHeader("Message", e.getMessage());
			return;
		}
		response.setStatus(HttpServletResponse.SC_OK);
	}

	@RequestMapping(path = "/{user}", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable String user, HttpServletResponse response, HttpSession session)
			throws IllegalAccessException {
		validation.validateAccess(session, response);
		if (!user.equals(((User) session.getAttribute("user")).getUsername())) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.setHeader("Message", "One user cannot delete another");
			return;
		}
		userService.deleteUser(user);
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}

	@RequestMapping(path = "/me", method = RequestMethod.DELETE)
	public void deleteMe(HttpServletResponse response, HttpSession session) throws IllegalAccessException {
		validation.validateAccess(session, response);
		userService.deleteUser((User) session.getAttribute("user"));
		response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	}

}
