package net.krupizde.movieratingapp.rest.controllers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.krupizde.movieratingapp.entities.Comment;
import net.krupizde.movieratingapp.entities.Movie;
import net.krupizde.movieratingapp.entities.User;
import net.krupizde.movieratingapp.entities.dto.MovieDto;
import net.krupizde.movieratingapp.service.MovieService;
import net.krupizde.movieratingapp.service.StudioService;
import net.krupizde.movieratingapp.service.ValidationService;

@RestController
@RequestMapping(path = "/movies")
public class MovieController {

	@Autowired
	MovieService movieService;

	@Autowired
	StudioService studioService;

	@Autowired
	ValidationService validation;

	@RequestMapping(path = "/{from}/{to}", method = RequestMethod.GET)
	public List<Movie> getAll(@PathVariable long from, @PathVariable long to, HttpSession session) throws Exception {
		validation.validateAccess(session);
		return movieService.loadAll();
	}

	@RequestMapping(path = "",method = RequestMethod.GET)
	public List<Long> getAllIds(HttpSession session) throws Exception {
		validation.validateAccess(session);
		return movieService.loadAll().stream().map(f->{return f.getId();}).collect(Collectors.toList());
	}
	
	@RequestMapping(path = "/rated", method = RequestMethod.GET)
	public List<Movie> getRated(HttpSession session) throws IllegalAccessException {
		validation.validateAccess(session);
		List<Movie> movies = movieService.userRatedMovies((User) session.getAttribute("user"));
		return movies;
	}

	@RequestMapping(method = RequestMethod.POST)
	public MovieDto add(@RequestBody MovieDto movie, HttpServletResponse response, HttpSession session)
			throws IllegalAccessException {
		validation.validateAccess(session, response);
		Movie m = new Movie(studioService.findById(movie.getIdStudio()), null, null, movie.getId(), movie.getName(),
				movie.getLengthMinutes(), null, null);
		movieService.save(m);
		return movieService.findByName(movie.getName()).reduce();
	}

}
