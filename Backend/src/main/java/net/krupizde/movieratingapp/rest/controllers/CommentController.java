package net.krupizde.movieratingapp.rest.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.krupizde.movieratingapp.entities.Comment;
import net.krupizde.movieratingapp.entities.User;
import net.krupizde.movieratingapp.entities.dto.CommentDto;
import net.krupizde.movieratingapp.service.CommentService;
import net.krupizde.movieratingapp.service.ValidationService;

@RestController
@RequestMapping(path = "/comments")
public class CommentController {
	
	@Autowired
	CommentService commentService;
	@Autowired
	ValidationService validation;

	@RequestMapping(path="/movie/{id}", method = RequestMethod.GET)
	public List<Comment> getAllComments(@PathVariable long id, HttpSession session) throws IllegalAccessException {
		validation.validateAccess(session);
		return commentService.findByMovie(id);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public void addComment(@RequestBody CommentDto commentDto, HttpSession session, HttpServletResponse response) throws IllegalAccessException {
		validation.validateAccess(session, response);
		try {
			commentService.addComment(commentDto, (User) session.getAttribute("user"));
			response.setStatus(HttpServletResponse.SC_CREATED);
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Message", e.getMessage());
			return;
		}
	}

}
