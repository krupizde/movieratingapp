package net.krupizde.movieratingapp.service;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

@Service
public class ValidationService implements IService {

	public void validateAccess(HttpSession session, HttpServletResponse response) throws IllegalAccessException {
		if(session.getAttribute("user") == null) {
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			throw new IllegalAccessException("User not logged in");
		}
	}
	
	public void validateAccess(HttpSession session) throws IllegalAccessException {
		if(session.getAttribute("user") == null) {
			throw new IllegalAccessException("User not logged in");
		}
	}
}
