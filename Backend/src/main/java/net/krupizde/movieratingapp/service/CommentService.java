package net.krupizde.movieratingapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.krupizde.movieratingapp.entities.Comment;
import net.krupizde.movieratingapp.entities.User;
import net.krupizde.movieratingapp.entities.dto.CommentDto;
import net.krupizde.movieratingapp.repository.CommentRepository;

@Service
public class CommentService implements IService{

	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	MovieService movieService;
	
	public List<Comment> loadAll(){
		return commentRepository.findAll();
	}
	public List<Comment> findByMovie(long id){
		return commentRepository.findByMovieIdOrderByTimeAdded(id);
	}
	
	public void addComment(CommentDto dto, User user) {
		Comment tmp = new Comment(0l, user, movieService.loadById(dto.getIdMovie()), dto.getContent(), dto.getTimeAdded());
		commentRepository.save(tmp);
	}
}
