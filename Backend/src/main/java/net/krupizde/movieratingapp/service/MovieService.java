package net.krupizde.movieratingapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.krupizde.movieratingapp.entities.Movie;
import net.krupizde.movieratingapp.entities.User;
import net.krupizde.movieratingapp.repository.MovieRepository;

@Service
public class MovieService implements IService{

	@Autowired
	MovieRepository movieRepository;


	public List<Movie> loadAll() {
		return movieRepository.findAll();
	}

	public List<Movie> userRatedMovies(User user){
		return movieRepository.findByUserRatingsUserId(user.getId());
	}
	
	public Movie loadById(long id) {
		return movieRepository.getOne(id);
	}
	
	public void save(Movie m) {
		movieRepository.save(m);
	}
	
	public Movie findByName(String name) {
		return movieRepository.findByName(name).get(0);
	}
}
