package net.krupizde.movieratingapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javassist.NotFoundException;
import net.krupizde.movieratingapp.entities.Movie;
import net.krupizde.movieratingapp.entities.User;
import net.krupizde.movieratingapp.entities.UserRating;
import net.krupizde.movieratingapp.entities.dto.UserRatingDto;
import net.krupizde.movieratingapp.entities.ids.UserRatingsId;
import net.krupizde.movieratingapp.repository.MovieRepository;
import net.krupizde.movieratingapp.repository.UserRatingsRepository;
import net.krupizde.movieratingapp.repository.UserRepository;

@Service
public class UserRatingsService implements IService{

	@Autowired
	UserRatingsRepository userRatingsRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	MovieRepository movieRepository;
	
	public List<UserRating> loadAll(){
		return userRatingsRepository.findAll();
	}
	
	public void addRating(UserRatingDto dto, User u){
		UserRatingsId id = new UserRatingsId();
		id.setIdMovie(dto.getMovieId());
		id.setIdUser(u.getId());
		Movie m = movieRepository.findById(dto.getMovieId()).get();
		UserRating rating = new UserRating(id, u, m, dto.isFavorite(), dto.getRating());
		userRatingsRepository.save(rating);
	}
}
