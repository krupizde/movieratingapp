package net.krupizde.movieratingapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.krupizde.movieratingapp.entities.Tag;
import net.krupizde.movieratingapp.repository.TagRepository;

@Service
public class TagService implements IService{

	@Autowired
	TagRepository tagRepository;
	
	public List<Tag> loadAll(){
		return tagRepository.findAll();
	}
}
