package net.krupizde.movieratingapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.krupizde.movieratingapp.entities.User;
import net.krupizde.movieratingapp.repository.UserRepository;

@Service
public class UserService implements IService{

	@Autowired
	UserRepository userRepository;
	
	public List<User> loadAll(){		
		return userRepository.findAll();
	}
	
	public User findByUsername(String username) {
		User tmp = userRepository.findByUsername(username).get(0);
		return tmp;
	}
	
	public void register(User u) {
		userRepository.save(u);
	}
	
	public void deleteUser(User u) {
		userRepository.delete(u);
	}
	
	public void deleteUser(String username) {
		userRepository.delete(userRepository.findByUsername(username).get(0));
	}
}
