package net.krupizde.movieratingapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.krupizde.movieratingapp.entities.Studio;
import net.krupizde.movieratingapp.repository.StudioRepository;

@Service
public class StudioService implements IService{
	
	@Autowired
	StudioRepository studioRepository;
	
	public List<Studio> loadAll(){
		return studioRepository.findAll();
	}
	
	public void add(Studio s) {
		studioRepository.save(s);
	}
	
	public Studio findByName(String name) {
		return studioRepository.findByName(name).get(0);
	}
	
	public Studio findById(long id) {
		return studioRepository.findById(id).get();
	}
}
