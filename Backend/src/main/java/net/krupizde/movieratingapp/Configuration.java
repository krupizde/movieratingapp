package net.krupizde.movieratingapp;

import org.springframework.context.annotation.ComponentScan;

@org.springframework.context.annotation.Configuration
@ComponentScan(basePackages = {"net.krupizde.movieratingapp"})
public class Configuration {

}
