package net.krupizde.movieratingapp.jackson.serializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import net.krupizde.movieratingapp.entities.Comment;

public class CommentSerializer extends StdSerializer<Comment>{

	private static final long serialVersionUID = 1L;

	public CommentSerializer() {
		this(null);
	}

	public CommentSerializer(Class<Comment> t) {
		super(t);
	}


	@Override
	public void serialize(Comment value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeStartObject();
		gen.writeNumberField("id", value.getId());
		gen.writeStringField("content", value.getContent());
		gen.writeObjectFieldStart("user");
		gen.writeNumberField("idUser", value.getUser().getId());
		gen.writeStringField("username", value.getUser().getUsername());
		gen.writeEndObject();
		gen.writeObjectFieldStart("movie");
		gen.writeNumberField("idMovie", value.getMovie().getId());
		gen.writeStringField("name", value.getMovie().getName());
		gen.writeEndObject();
		gen.writeEndObject();
	}
}
