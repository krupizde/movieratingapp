/**
 * 
 */
package net.krupizde.movieratingapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Zdenek Krupicka (MrScruf) radamanak@gmail.com
 *
 */
@SpringBootApplication
public class Main {


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);	

	}

}
