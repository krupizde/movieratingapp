package movieratingapp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import net.krupizde.movieratingapp.Configuration;
import net.krupizde.movieratingapp.entities.User;
import net.krupizde.movieratingapp.service.PasswordService;
import net.krupizde.movieratingapp.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {Configuration.class})
public class ServiceTest {

	@Autowired
	PasswordService passwordService;

	@Autowired
	UserService userService;
	
	@Test
	public void passwordHash() {
		String password = "testovacíHesloBezMezerASZnakyLol_123", username = "TestovacíUsername";
		String passHash = passwordService.hashPassword(password);
		userService.register(new User(new Long(0), username, passHash, null, null));
		User u = userService.findByUsername(username);
		String hashAgain = passwordService.hashPassword(password);		
		Assert.assertEquals(hashAgain, u.getPasswordHash());
		userService.deleteUser(u);
	}
}
