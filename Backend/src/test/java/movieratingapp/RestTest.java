package movieratingapp;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.Cookie;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockCookie;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.krupizde.movieratingapp.Configuration;
import net.krupizde.movieratingapp.entities.dto.UserDto;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ContextConfiguration(classes = { Configuration.class })
@AutoConfigureMockMvc
public class RestTest {

	@Autowired
	private MockMvc mvc;
	


	@Test
	public void registerLogin() throws Exception {
		MvcResult res = mvc
				.perform(MockMvcRequestBuilders.post("/users").contentType(MediaType.APPLICATION_JSON)
						.content(asJsonString(new UserDto(0, "testik", "heslo123"))))
				.andExpect(status().isCreated()).andReturn();
		res = mvc
				.perform(MockMvcRequestBuilders.post("/users/login").contentType(MediaType.APPLICATION_JSON)
						.content(asJsonString(new UserDto(0, "testik", "heslo123"))))
				.andExpect(status().isOk()).andReturn();
		Cookie[] cookie = res.getResponse().getCookies();
		res = mvc.perform(MockMvcRequestBuilders.delete("/users/me")
				.contentType(MediaType.APPLICATION_JSON).cookie(cookie)).andExpect(status().isNoContent()).andReturn();

	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
